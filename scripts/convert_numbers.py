#!/usr/bin/env python3

import os
import string
import sys
from typing import List

EXTRA_CHARACTERS = [
    ",",
    ".",
    "،",
    " ِ",
    " َ",
    " ُ",
    'ِ',
    ' ّ',
    'ئ',
    "(",
    ")",
    "@",
    "»",
    "«",
    "–",
    "-",
    'َ',
    ":",
    'ي',
    'ً',
    'ك',
    "\uf0b7",
    "\U001001a9",
    "…",
    "/",
    'ء',
    "%",
    'ء',
    '؛',
    ":",
    ";",
    "?",
    "=",
    "&",
    'ھ',
    'σ',
    '۱',
    '٢',
    '٣',
    '۴',
    '۵',
    '۶',
    '۷',
    '٨',
    '٩',
    '٠',
    '١',
    'π',
    'Σ',
    '؟',
    "\U00101002",
    '×',
    'ؤ',
    'أ',
    'τ',
    'φ'
]


def main():

    # If not enough arguments are provided, print error and exit
    if len(sys.argv) < 4:
        print("Usage: convert_numbers.py <infile> <outfile> <alphabet file>")
        exit(1)
    
    alphabet = load_alphabet(sys.argv[3])
    print(alphabet)

    first = True
    with open(sys.argv[1], encoding="utf-8") as infile:
        with open(sys.argv[2], "w", encoding="utf-8") as outfile:
            for line in infile.readlines():
                for char in line:
                    if first:
                        first = False
                    else:
                        outfile.write(" ")
                    if char not in alphabet:
                        alphabet.append(char)
                    number = alphabet.index(char) + 1
                    outfile.write(str(number))

    os.makedirs("out", exist_ok=True)
    with open("out/alphabet.txt", "a", encoding="utf-8") as alphabet_file:
        for char in alphabet:
            alphabet_file.write(f"{char}\n")
        

def load_alphabet(path: str) -> List[str]:
    """Creates the alphabet list"""
    # Initialise the alphabet with a newline as index 0
    alphabet = [ "\n" ]

    for char in string.ascii_letters:
        alphabet.append(char)
    # Add numbers
    for i in range(0, 10):
        alphabet.append(str(i))
    
    # Load the rest of the alphabet from the alphabet file
    with open(path, encoding="utf-8") as alphabet_file:
        for line in alphabet_file.readlines():
            line = line.replace("\n", "")
            # Skip empty lines or lines starting with #
            if line.startswith("#") or not line:
                continue
            alphabet.append(line)

    return alphabet






if __name__ == "__main__":
    main()