#!/usr/bin/env python3

from enum import Enum, auto
import re

FOLDER = "data/persian/barzan/barzan_marked"
LETTER = re.compile(r"[A-Za-z0-9]")
CLOSING_BRACKET = re.compile(r"<\\[A-Z]>")

class Language(Enum):
    P = auto()
    E = auto()


def main():
    import pandas as pd
    df = pd.DataFrame(
        columns=[ "A", "P" ]
    )

    for fold in range(1,6):
        truth = open(f"{FOLDER}/fold_{fold}/truth.txt", encoding="utf8")
        marked = open(f"{FOLDER}/fold_{fold}/marked.txt", encoding="utf8")

        actual_lang = Language.P
        marked_lang = Language.E

        for (actual, prediction) in zip(truth, marked):
            actual = actual.strip()
            prediction = prediction.strip()

            actual_letters = ""
            for c in actual:
                if c == "<":
                    actual_lang = Language.E
                elif c == ">":
                    actual_lang = Language.P
                elif LETTER.match(c):
                    actual_letters += str(actual_lang.name)
                else:
                    actual_letters += c
            
            print("A:", actual)
            print("A:", actual_letters)

            prediction = CLOSING_BRACKET.sub("", prediction)
            print("P:", prediction)
            prediction_letters = ""
            while prediction:
                c = prediction[0]
                prediction = prediction[1:]
                if c == "<":
                    marked_lang = Language[prediction[0].upper()]
                    prediction = prediction[2:]
                elif LETTER.match(c):
                    prediction_letters += str(marked_lang.name)
                else:
                    prediction_letters += c
            
            print("P:", prediction_letters)

            assert len(actual_letters) == len(prediction_letters)
            df2 = pd.DataFrame(
                [ (a, p) for (a, p) in zip(actual_letters, prediction_letters) if a == "E" or a == "P" ],
                columns=["A", "P"]
            )
            df = pd.concat([df, df2])
            print()

        truth.close()
        marked.close()

    matrix = pd.crosstab(df['A'], df['P'])
    correct = 0
    for lang in Language:
        correct += matrix.at[lang.name, lang.name]

    total = matrix.values.sum()
    print(f"Total: {total}")
    print(f"Correct: {correct}")
    print(f"Accuracy: {correct*100/total:.2f}%")

    english = matrix.loc['E'].sum()
    print(f"Zeroshot: {(total-english)*100/total:.2f}%")

    print(matrix)


if __name__ == "__main__":
    main()
