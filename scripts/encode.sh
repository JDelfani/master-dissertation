#!/usr/bin/bash

encode < $2 -O $1 -p 1000 2>&1 >/dev/null | awk -v OFS='\t' "{print \$2, \$9}"