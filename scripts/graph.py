#!/usr/bin/env python3

import pandas as pd
import math
import sys
import matplotlib.pyplot as plt
import os

def capitalise(s: str) -> str:
    s = s[0].upper() + s[1:]
    return s.replace("_", " ")

def main():
   
    if len(sys.argv) <2:
        print("usage: graph.py <tsv file>")
        exit(1)
    file= os.path.split(sys.argv[1])[1].split(".")[0]

    df = pd.read_csv(sys.argv[1], sep="\t")

    lang1 = df.columns[2]
    lang2 = df.columns[3]
    print(lang1, lang2)

    df.plot(x= 2, y= 3, kind= "scatter")
    plt.axline([0, 0], [1, 1])
    
    plt.xlabel(f"{capitalise(lang1)} compression codelength")
    plt.ylabel(f"{capitalise(lang2)} compression codelength")

    max_value = max(df[lang1].max(), df[lang2].max())
    max_value = math.ceil(max_value / 100) * 100
    plt.xlim(0, max_value)
    plt.ylim(0, max_value)

    os.makedirs("figures", exist_ok=True)
    plt.savefig(f"figures/{file}.png")

    # Add a new column with the ratios
    df['ratio'] = df.apply(
        lambda row: row[lang2] / row[lang1],
        axis=1
    )

    # Fetch cases where the codelength is larger for
    # for one language over another
    lang2_count = len(df[ df['ratio'] > 1 ])
    lang1_count = len(df[ df['ratio'] < 1 ])
    same = len(df[ df['ratio'] == 1 ])

    # Assert that the number of rows found matches the total
    assert(len(df) == lang2_count + lang1_count + same)

    # Print results
    print(f"Codelength stats: {file}")
    print(f"{capitalise(lang2)}: {lang2_count:,} {capitalise(lang1)}: {lang1_count:,} Same {same} Total: {lang2_count + lang1_count + same:,}")
    print(f"{capitalise(lang2)}: {lang2_count/len(df)*100:.2f}% {capitalise(lang1)}: {lang1_count/len(df)*100:.2f}%")
    print()



if __name__ == "__main__":
    main()