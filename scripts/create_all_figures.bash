#!/usr/bin/bash

for english in {6..7}
do
    for persian in {6..6}
    do
        scripts/graph.py out/$english-$persian.tsv
    done
done
