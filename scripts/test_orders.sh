#!/usr/bin/bash


mkdir -p out
filename=`basename -- $1 | cut -d'.' -f1`
out="out/$filename"
for i in {1..10}
do
    echo -e "Bytes\tOrder $i" > $out.$i.tsv
    ./encode.sh $i $1 >> $out.$i.tsv
done