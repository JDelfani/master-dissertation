#!/usr/bin/env python3
import pandas as pd
from scipy import stats
import argparse

def main():

    parser = argparse.ArgumentParser(description="Calculates the Pearson's correlation coefficient between two columns in a CSV file")
    parser.add_argument('csv_file', help='The file to process')
    parser.add_argument('--col1', default=2, type=int, help='The first column (index)')
    parser.add_argument('--col2', default=3, type=int, help='The first column (index)')
    args = parser.parse_args()

    df = pd.read_csv(args.csv_file, sep="\t")
    corr, p = stats.pearsonr(df.iloc[:, args.col1], df.iloc[:, args.col2])
    print(f"Column 1: {df.columns[args.col1]} Column 2: {df.columns[args.col2]}")
    print(f"Correlation: {corr:.3f} P-Value: {p:.3f}")

if __name__ == "__main__":
    main()