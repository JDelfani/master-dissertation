#!/usr/bin/env python3

import sys
import re
from typing import Dict


NEW_ENTRY = re.compile(r"^\(([0-9]+)\)\s+")


def main() -> None:
    """Main function of the program"""
    if len(sys.argv) < 3:
        print("usage: filter_barzan.py <barzan file> <out file>")
        sys.exit(1)
    # Dictionary to store the results
    lines: Dict[int, str] = {}

    # Open the barzan file
    with open(sys.argv[1], encoding="utf-8") as file:
        current = None
        # Processes all the lines
        for line in file.readlines():
            # Checks whether the line starts with a number
            match = NEW_ENTRY.match(line)

            # If the line does not contain a backtick or
            # start with a number, continue
            if "`" not in line and not match:
                continue

            # Remove backticks and extra whitespace
            line = line.replace("`", "").strip()

            # If line starts with a number add it to
            # the dictionary
            if match:
                # Extracts the number
                current = int(match.group(1))
                # Removes the number from the sentence
                line = line.replace(match.group(0), "")
                lines[current] = line
            # Otherwise add to previous line
            else:
                lines[current] += " " + line

    # Ensure that all lines are included
    for i in range(1, max(lines.keys()) + 1):
        if i not in lines:
            raise Exception(f"Line {i} was not found in the list of lines")
    
    # Write to file
    with open(sys.argv[2], "w", encoding="utf-8") as out:
        for (_, sent) in sorted(lines.items(), key=lambda x: x[0]):
            out.write(f"{sent}\n")


if __name__ == "__main__":
    main()
