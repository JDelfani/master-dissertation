#!/usr/bin/bash
# trainmodel.sh <order> <folder>

for fold in $2/*
do
    rm -f $fold/fold.model $fold/fold_o$1.model
    train -i $fold/train.txt -N -o $fold/fold_o$1.model -p 100000 -a 256 -O $1 -e 'C' -T "order $1 model" -S 
done

