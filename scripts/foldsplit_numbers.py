#!/usr/bin/env python3

from io import TextIOWrapper
import os
import sys
from typing import Iterable, List

# foldsplit.py 5 myfile.txt

# N-chunks
# File

class Fold:

    def __init__(self, number: int, prefix: str) -> None:
        """Creates the object and opens up the training and test
        files so that they can be written to. Also creates the folders
        if required.

        Args:
            number (int): the number of the fold
            prefix (str): the prefixed folder name
        """
        self.number = number
        folder = f"{prefix}/fold_{number + 1}"
        os.makedirs(folder, exist_ok=True)
        self.train = open(f"{folder}/train.txt", "w")
        self.test = open(f"{folder}/test.txt", "w")


    def add_data(self, fold: int, data: str):
        """Adds the data to the fold. If the fold matches
        the number of the fold, it is added to the test set,
        otherwise it is added to the training set.

        Args:
            fold (int): current fold
            data (str): current row / sentence
        """
        data += " "
        if fold == self.number:
            self.test.write(data)
        else:
            self.train.write(data)



def main():

    # If not enough arguments are provided, print error and exit
    if len(sys.argv) < 3:
        print("Usage: foldsplit.py <chunks> <file>")
        exit(1)

    # Fetch arguments
    chunks = int(sys.argv[1])
    file = sys.argv[2]

    # Get the prefix from the file name
    parts = os.path.split(file)[1].split(".")
    if len(parts) > 1:
        parts = parts[:-1]
    prefix = ".".join(parts)

    # Create the folds
    folds = [ Fold(x, prefix) for x in range(chunks) ]

    # Read the data
    with open(file) as infile:
        for index, row in enumerate(get_next_sentence(infile)):
            # Add the data to all of the folds
            data =  " ".join(row)
            fold_nr = index % chunks
            for fold in folds:
                fold.add_data(fold_nr, data)

def get_next_number(file: TextIOWrapper) -> List[str]:
    current = ""
    while True:
        c = file.read(1)
        if not c:
            break
        if c == " " and current:
            yield current
            current = ""
        elif c != " ":
            current += c

    if len(current) > 0:
        yield current



def get_next_sentence(file) -> Iterable[str]:
    current = []
    for number in get_next_number(file):
        number = number.strip()
        if number:
            current.append(number)
        if number == "1":
            yield current
            current = []
    if len(current) > 0:
        yield current


if __name__ == "__main__":
    main()