#!/usr/bin/env python3

import argparse
import sys
import os
from model_server import ModelServer
from foldsplit_numbers import get_next_sentence

def get_folds(folder1: str, folder2: str):
    """Returns a list of folds in the two folders"""
    s1= set(os.listdir(folder1))
    s2= set(os.listdir(folder2))
    return s1.intersection(s2)

def main():
    """The main function of the program"""
    if len(sys.argv) <=4:
        print("usage: test.py [-N] <order1> <folder1> <order2> <folder2>")
        exit(1)

    # Fetching the arguments of the program
    parser = argparse.ArgumentParser()
    parser.add_argument('-N', action='store_true')
    parser.add_argument("order1")
    parser.add_argument("folder1")
    parser.add_argument("order2")
    parser.add_argument("folder2")
    options = parser.parse_args()

    if options.N:
        print("N-flag detected. Processing folder2 as a series of numbers")

    args = sys.argv[-4:]
    order1= int(args[0])
    order2= int(args[2])
    folder1= args[1]
    folder2= args[3]

    folds= get_folds(folder1, folder2)

    port = 16000

    lines = ["fold\tsentence\tenglish\tpersian\n"]

    persian_args = ["-N"] if options.N else []

    for fold in sorted(folds):
        print(f"Processing fold: {fold}")
        test1= open(f"{folder1}/{fold}/test.txt", encoding="utf-8")
        test2= open(f"{folder2}/{fold}/test.txt", encoding="utf-8")
        print(f"Test files: {test1.name} | {test2.name}")
        model1= ModelServer(f"{folder1}/{fold}/fold_o{order1}.model", port= port)
        model2= ModelServer(f"{folder2}/{fold}/fold_o{order2}.model", port= port+1, args=persian_args)
        
        port += 2
        i= 0
        total = 204319

        file2iter = get_next_sentence(test2) if options.N else None
        
        while True:
            i+= 1
            line1=test1.readline()
            line2 = " ".join(next(file2iter, [])) if file2iter else test2.readline()
            if file2iter is not None:
                line2 = line2.strip()
            
            # Ensure that both files finishes at the same time
            if ((not line1) + (not line2)) == 1:
                raise Exception(f"reached end of line: {line1!r} | {line2!r}")

            # If both lines are empty, exit
            if not line1 or not line2:
                break
            cl1= model1.get_codelength(line1)
            cl2= model2.get_codelength(line2)
            if i % 100 == 0 or i == total:
                print(f"{i / total * 100:.2f}% | {i}/{total}", end="\r")
            lines.append(f"{fold}\t{i}\t{cl1}\t{cl2}\n")

        print("\n")
       
        with open(f"out/{order1}-{order2}.tsv", "w", encoding="utf-8") as outfile:
            outfile.writelines(lines)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(e)