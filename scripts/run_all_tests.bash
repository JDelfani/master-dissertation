#!/usr/bin/bash

for english in {7..7}
do
    for persian in {6..6}
    do
        scripts/test.py -N $english data/persian/mizan/mizan_en $persian data/persian/mizan/mizan_numbers
    done
done
