#!/usr/bin/bash

# Usage: test_bpc.sh <min order> <max order> <folder>
# Example: test_bpc 5 6 data/persian/mizan/mizan_en

for ((order=$1;order<=$2;order++))
do
    list=()
    for fold in $3/*
    do
        echo "Testing model: $fold/fold_o$order.model"
        bpc=$(encode < $fold/test.txt -m $fold/fold_o$order.model -p 100000 2>&1 >/dev/null | awk -v OFS='\t' "{current = \$9} END {print current}")
        echo "BPC: " $bpc
        list+=($bpc)
    done
    echo ${list[@]} | python3 -c "values = list(map(float, input().split())); print(f\"{sum(values)/len(values):.4f}\")"
    echo -e "Finished with order " $order "\n"
done
