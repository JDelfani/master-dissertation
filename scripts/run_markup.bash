
for fold in $1/*
do
    echo "E  	data/english/brown.model" > $fold/models.dat
    echo "P  	$fold/fold_o4.model" >> $fold/models.dat
    markup -m $fold/models.dat -p 100 -F -V < $fold/test.txt > $fold/marked.txt
done

