#!/usr/bin/env python3

import os
import sys

# foldsplit.py 5 myfile.txt

# N-chunks
# File

class Fold:

    def __init__(self, number: int, prefix: str) -> None:
        """Creates the object and opens up the training and test
        files so that they can be written to. Also creates the folders
        if required.

        Args:
            number (int): the number of the fold
            prefix (str): the prefixed folder name
        """
        self.number = number
        folder = f"{prefix}/fold_{number + 1}"
        os.makedirs(folder, exist_ok=True)
        self.train = open(f"{folder}/train.txt", "w")
        self.test = open(f"{folder}/test.txt", "w")


    def add_data(self, fold: int, data: str):
        """Adds the data to the fold. If the fold matches
        the number of the fold, it is added to the test set,
        otherwise it is added to the training set.

        Args:
            fold (int): current fold
            data (str): current row / sentence
        """
        if fold == self.number:
            self.test.write(data)
        else:
            self.train.write(data)



def main():

    # If not enough arguments are provided, print error and exit
    if len(sys.argv) < 3:
        print("Usage: foldsplit.py <chunks> <file>")
        exit(1)

    # Fetch arguments
    chunks = int(sys.argv[1])
    file = sys.argv[2]

    # Get the prefix from the file name
    parts = os.path.split(file)[1].split(".")
    if len(parts) > 1:
        parts = parts[:-1]
    prefix = ".".join(parts)

    # Create the folds
    folds = [ Fold(x, prefix) for x in range(chunks) ]

    # Read the data
    with open(file) as input:
        # Enumerate each line
        for index, row in enumerate(input.readlines()):
            # Add the data to all of the folds
            fold_nr = index % chunks
            for fold in folds:
                fold.add_data(fold_nr, row)


if __name__ == "__main__":
    main()